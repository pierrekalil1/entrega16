from app.configs.database import db
from dataclasses import dataclass
from app.models.tasks_categories_model import tasks_categories

@dataclass
class CategoryModel(db.Model):

    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    description = db.Column(db.Text)

    task = db.relationship(
        'TasksModel',
        secondary=tasks_categories,
        backref='category'
    )