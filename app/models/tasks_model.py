from sqlalchemy.orm import backref, relationship
from app.configs.database import db
from dataclasses import dataclass

@dataclass
class TasksModel(db.Model):

    __tablename__ = 'tasks'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    description = db.Column(db.String)
    duration = db.Column(db.Integer)
    importance = db.Column(db.Integer)
    urgency = db.Column(db.Integer)

    eisenhower_id = db.Column(
        db.Integer, 
        db.ForeignKey('eisenhowers.id'), 
        nullable=False
    )
    # eisenhower_classification = relationship('EisenhowersModel', backref=backref('tasks', uselist=False), uselist=False)