from sqlalchemy.orm import relationship
from app.configs.database import db

class EisenhowersModel(db.Model):

    __tablename__ = 'eisenhowers'

    id = db.Column(db.Integer, primary_key=True)
    type_task = db.Column(db.String(100))

    tasks = relationship("TasksModel", backref="eisenhowers",uselist=True)