from flask import request, current_app, jsonify
from ipdb.__main__ import set_trace
from app.exc.exceptions import InvalidOptionsError, NoneTypeExceptError
from app.models.category_model import CategoryModel
from app.models.eisenhowers_model import EisenhowersModel
from app.models.tasks_model import TasksModel
from http import HTTPStatus
from sqlalchemy.exc import IntegrityError

def create_task():
    try:
        session = current_app.db.session

        data = request.get_json()

        if data['importance'] > 2 or data['urgency'] > 2:
            raise InvalidOptionsError(data['importance'], data['urgency'])
        
        if data['importance'] == 1 and data['urgency'] == 1:
            data['eisenhower_id'] = 1
        if data['importance'] == 1 and data['urgency'] == 2:
            data['eisenhower_id'] = 2
        if data['importance'] == 2 and data['urgency'] == 1:
            data['eisenhower_id'] = 3
        if data['importance'] == 2 and data['urgency'] == 2:
            data['eisenhower_id'] = 4

        type = data['eisenhower_id']
        task_type = EisenhowersModel.query.filter_by(id=type).first()

        categories_list = data.pop('categories')
        

        task = TasksModel(**data)

        for cat1 in categories_list:
            for cat2 in cat1.values():
                filti = CategoryModel.query.filter_by(name=cat2)
                for caty in filti:
                    if cat2 == caty.name:
                        return jsonify({"error": f"Category {cat2} aleary exists"})

        for category in categories_list:
            category_obj = CategoryModel(**category)
            task.category.append(category_obj)

        session.add(task)
        session.commit()

        return jsonify({
            "id": task.id,
            "name": task.name,
            "description": task.description,
            "duration": task.duration,
            "eisenhower_classification": task_type.type_task,
            "category": categories_list
        }), HTTPStatus.OK

    except InvalidOptionsError as e:
        return jsonify(e.message), HTTPStatus.CONFLICT
    except IntegrityError:
        return jsonify({"msg": f"Task '{task.name}' aleary exists"}), HTTPStatus.CONFLICT


def update_task(task_id):
    try:
        session = current_app.db.session

        data = request.get_json()

        if 'importance' in data and 'urgency' in data:
            if data['importance'] > 2 or data['urgency'] > 2:
                raise InvalidOptionsError(data['importance'], data['urgency'])
        
            if data['importance'] == 1 and data['urgency'] == 1:
                data['eisenhower_id'] = 1
            if data['importance'] == 1 and data['urgency'] == 2:
                data['eisenhower_id'] = 2
            if data['importance'] == 2 and data['urgency'] == 1:
                data['eisenhower_id'] = 3
            if data['importance'] == 2 and data['urgency'] == 2:
                data['eisenhower_id'] = 4
            type = data['eisenhower_id']
            task_type = EisenhowersModel.query.filter_by(id=type).first()

        if 'urgency' not in data:
            if data['importance'] > 2:
                return jsonify({"error": f"Value {data['importance']} not valid. Values acepts: 1 or 2"})
            urgency = TasksModel.query.get(task_id)
            if data['importance'] == 1 and urgency.urgency == 1:
                data['eisenhower_id'] = 1
            if data['importance'] == 1 and urgency.urgency == 2:
                data['eisenhower_id'] = 2
            if data['importance'] == 2 and urgency.urgency == 1:
                data['eisenhower_id'] = 3
            if data['importance'] == 2 and urgency.urgency == 2:
                data['eisenhower_id'] = 4
            type = data['eisenhower_id']
            task_type = EisenhowersModel.query.filter_by(id=type).first()
        
        if 'importance' not in data:
            if data['urgency'] > 2:
                return jsonify({"error": f"Value {data['urgency']} not valid. Values acepts: 1 or 2"})
            importance = TasksModel.query.get(task_id)
            if data['urgency'] == 1 and importance.importance == 1:
                data['eisenhower_id'] = 1
            if data['urgency'] == 1 and importance.importance == 2:
                data['eisenhower_id'] = 2
            if data['urgency'] == 2 and importance.importance == 1:
                data['eisenhower_id'] = 3
            if data['urgency'] == 2 and importance.importance == 2:
                data['eisenhower_id'] = 4
            type = data['eisenhower_id']
            task_type = EisenhowersModel.query.filter_by(id=type).first()

        task = TasksModel.query.get(task_id)
  
        if task == None:
            raise NoneTypeExceptError

        task_type = EisenhowersModel.query.filter_by(id=task.eisenhower_id).first()

        for key, value in data.items():
            setattr(task, key, value)

        session.add(task)
        session.commit()

        return jsonify({
                "id": task.id,
                "name": task.name,
                "description": task.description,
                "duration": task.duration,
                "eisenhower_classification": task_type.type_task
            }), HTTPStatus.CREATED

    except NoneTypeExceptError:
        return jsonify({"msg": "category not found!"})


def delete_task(id):
    try:
        session = current_app.db.session
        task = TasksModel.query.get(id)

        if task == None:
            raise NoneTypeExceptError

        session.delete(task)
        session.commit()

        return jsonify(), HTTPStatus.NO_CONTENT
    
    except NoneTypeExceptError:
        return jsonify({"msg": "category not found!"})