from flask import request, current_app, jsonify
from app.models.category_model import CategoryModel
from http import HTTPStatus
# from app.exc import NoneTypeExceptError
from ipdb import set_trace
from app.models.eisenhowers_model import EisenhowersModel
from app.models.tasks_categories_model import tasks_categories
from sqlalchemy.exc import IntegrityError

from app.models.tasks_model import TasksModel

def create_category():
    session = current_app.db.session

    data = request.get_json()

    list_category = (
            CategoryModel
            .query
            .all()
        )

    seralizer = [
        {
            "name": item.name,
            "description": item.description
        }for item in list_category
    ]

    list_item = list()
    for value in seralizer:
        for v in value.values():
            list_item.append(v)
            
    if data['name'] in list_item or data['description'] in list_item:
        return {"error": f"Name: {data['name']} and Email: {data['description']}, aleary exists"}, 409

    category = CategoryModel(**data)

    session.add(category)
    session.commit()

    return jsonify({
        "id": category.id,
        "name": category.name,
        "description": category.description
    }), HTTPStatus.CREATED


def get_all():

    categories = CategoryModel.query.join(tasks_categories).join(TasksModel).all()

    seralizer = [
        {
            "id": category.id,
            "name": category.name,
            "description": category.description,
            "tasks": [
                {
                "id": task.id,
                "name": task.name,
                "description": task.description,
                "priority": task.eisenhowers.type_task

                } for task in category.task]
        }for category in categories
    ]
    return {"categories": seralizer}


def update_category(id):
    
    data = request.get_json()

    category = CategoryModel.query.get(id)

    if category == None:
        return {"error": f"ID '{id}' not found"}

    for key, value in data.items():
        setattr(category, key, value)

    current_app.db.session.add(category)
    current_app.db.session.commit()

    return jsonify({
        "id": category.id,
        "name": category.name,
        "description": category.description
        }),HTTPStatus.OK
   

def delete_category(id):

    list_category = (
            CategoryModel
            .query
            .all()
        )

    seralizer = [
        {
            "id": item.id
        }for item in list_category
    ]

    list_item = list()
    for value in seralizer:
        for v in value.values():
            list_item.append(v)
    
    if id not in list_item:
       return {"error": f"Not found ID {id} in database"}, HTTPStatus.NOT_FOUND

    category = CategoryModel.query.get(id)

    current_app.db.session.delete(category)
    current_app.db.session.commit()

    return {}, HTTPStatus.NO_CONTENT
  