class NoneTypeExceptError(Exception):
    ...


class InvalidOptionsError(Exception):
    urgency = [1,2]
    importance = [1,2]
    def __init__(self, impo, urg) -> None:
        self.impo = impo
        self.urg = urg
        self.message = {
            "error":{
              "valid_options": {
                "importance": self.importance,
                "urgency": self.urgency
              },
              "recieved_options":{
                "importance": self.impo,
                "urgency": self.urg
              }
            }
          }

        super().__init__(self.message)


