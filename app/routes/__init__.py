from flask import Flask
from .api_blueprint import bp as api_bp


def init_app(app: Flask):
    app.register_blueprint(api_bp)
