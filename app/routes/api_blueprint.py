from flask import Blueprint
from .tasks_blueprint import bp as tasks_bp
from .category_blueprint import bp as category_bp

bp = Blueprint('api_bp', __name__, url_prefix='/api')

bp.register_blueprint(category_bp)
bp.register_blueprint(tasks_bp)