from flask import Blueprint
from app.controllers.category_controller import create_category, update_category, delete_category, get_all

bp = Blueprint("category_bp", __name__, url_prefix='/category')

bp.post("")(create_category)
bp.get("")(get_all)
bp.patch("/<int:id>")(update_category)
bp.delete("/<int:id>")(delete_category)